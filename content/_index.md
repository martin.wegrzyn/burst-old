![LOGO](burst_Logo_RGB_UE-14112021.svg "BURST")

## Willkommen!

auf der Website der ReproducibiliTea Gruppe Bielefeld

## Treffen

Das nächste Treffen des Bielefeld University Reproducible Science Tea's ist am   
**Freitag 29.04.2022** von **12:00** bis 13:00 vor Ort in **T2-214** und Online via Zoom  

Diskutiert und vorgestellt wird der Artikel [Simmons et al. (2011). False-Positive Psychology. Psychological Science, 22(11), 1359–1366.](https://dx.doi.org/10.1177/0956797611417632)

Zoom:
* [Link: Zoom-Meeting beitreten](https://uni-bielefeld.zoom.us/j/94847529577?pwd=b3lSeXEzbDg0RldUOFhJNmF2b1h3Zz09)
* Meeting-ID: 948 4752 9577  
* Passwort: 785514  
  
Wir freuen uns auf Eure Teilnahme!  

## Weitere Infos

* [ReproducibiliTea Webseite](https://reproducibilitea.org/journal-clubs/#Bielefeld)
* [Open Science Network](https://osf.io/k8npa/)
* [GitLab](https://gitlab.ub.uni-bielefeld.de/burst/burst)

## Kontakt

[burst@uni-bielefeld.de](mailto:burst@uni-bielefeld.de)
